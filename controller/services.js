const userModel = require('../schema/user').UserSchema;

class Services {
    findServices(req, res) {
        const serviceToFind = req.body.service;
        console.log('serviceToFind : : ', serviceToFind)
        let serviceProvider = [];
        userModel.find({
            services: {
                $elemMatch: {
                    name: serviceToFind
                }
            }
        }, (err, result) => {
            console.log('all services : : ', result)
            if (err) {
                return res.json({
                    success: false
                });
            } else {
                return res.json({
                    success: true,
                    data: result.map(provider => {
                        return {
                            "Adharcard": provider.Adharcard,
                            "Address": provider.Address,
                            "locations": provider.locations,
                            "mobileNo1": provider.mobileNo1
                        }
                    })
                })
            }
        })
    }
    findServiceByLocation(req, res) {
        const location = req.body.location;
        const service = req.body.service;
        userModel.find({
            "Address.city": location, services: {
                $elemMatch: {
                    name: service
                }
            }
        }, (err, result) => {
            if (err) {
                return res.json({
                    success: false,
                    data: err
                });
            } else {
                return res.json({
                    success: true,
                    data: result
                });
            }
        })
    }
}

module.exports = new Services();