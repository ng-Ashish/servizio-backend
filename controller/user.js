const otpSender = require('../msg91/msg91').MsgSender;
const userModel = require('../schema/user').UserSchema;
const otpModel = require('../schema/otp').OtpSchema;

class User {
    userRegistration(req, res) {
        const number = req.body.number;
        const countryCode = "91";
        console.log('req.body.number :', req.body.number)
        userModel.findOne({
            mobileNo1: "91" + number + ""
        }, (err, result) => {
            if (result !== null) {
                return res.json({
                    success: false,
                    message: 'user alredy present in system'
                });
            } else {
                const otp = Math.floor(100000 + Math.random() * 900000);
                console.log('otp :', otp, number, countryCode);
                /**
                 * send otp function
                 * for live use
                 */
                otpSender.sendOTP(number, countryCode, otp);
                // console.log('flag >', flag)
                /**
                 * after sending
                 */

                if (true) {
                    const userObj = new userModel();
                    userObj.mobileNo1 = countryCode + '' + number;
                    userObj.isVender = true;
                    userObj.status = true;
                    userObj.save((err, result) => {
                        if (err) {

                            return res.json({
                                success: false,
                                err: err
                            });
                        } else {
                            console.log('result :>>', result)
                            //5 min extra time
                            if (result !== null) {
                                const otpObj = new otpModel();
                                otpObj.userId = result._id;
                                otpObj.otpSentOn = result.mobileNo1;
                                otpObj.otp = otp;
                                otpObj.optTimestamp = new Date().getTime();
                                otpObj.otpexpiredOn = new Date().getTime() + 300000;
                                otpObj.save((err, result) => {
                                    if (err) {
                                        return res.json({
                                            success: false
                                        });
                                    } else {
                                        return res.json({
                                            success: true,
                                            message: 'otp is send to user'
                                        });
                                    }
                                })
                            }
                        }
                    })
                }
            }
        })
    }
    resendOtp(req, res) {
        const number = req.body.number;
        const countryCode = "91";
        otpModel.findOne({
            otpSentOn: number
        }, (err, result) => {
            if (result !== null) {
                var s = new Date().getTime();
                // console.log(result.otpexpiredOn, s, result.otpexpiredOn < s);
                if (!(result.otpexpiredOn < s)) {
                    return res.json({
                        success: true,
                        flag: 'existing_otp',
                        data: result.otp
                    });
                } else {
                    const otp = Math.floor(100000 + Math.random() * 900000);
                    console.log(otp)
                    const flag = otpSender.sendOTP(number, countryCode, otp);
                    if (true) {
                        otpModel.updateOne({
                            otpSentOn: number
                        }, {
                            $set: {
                                otp: otp,
                                optTimestamp: new Date().getTime(),
                                otpexpiredOn: new Date().getTime() + 300000
                            }
                        }, (err, result) => {
                            return res.json({
                                success: true,
                                flag: 'new_otp',
                                data: otp
                            });
                        })
                    }
                }
            } else {
                return res.json({
                    success: true,
                    message: 'new user'
                });
            }
        })
    }
    verifyOtp(req, res) {
        console.log('in verify otp');
        const otp = req.body.otp;
        const number = req.body.number;
        otpModel.findOne({
            otpSentOn: number
        }).sort({
            optTimestamp: -1
        }).limit(1).then(result => {
            if (result != null) {
                console.log(result, result.otp, '==', otp)
                if (result.otp == otp) {
                    return res.json({
                        success: true
                    });
                } else {
                    return res.json({
                        success: false
                    });
                }
            }
        })
    }
    test(req, res) {
        res.send({
            data: 'this.test'
        })
    }
    updateUser(req, res) {
        console.log(req.body)
        userModel.update({
            mobileNo1: req.body.mobilenumber
        }, {
            $set: {
                Adharcard: {
                    name: req.body.adharname,
                    number: req.body.adharnumber
                },
                Address: {
                    businessAddress: req.body.businessAddress,
                    landMark: req.body.landMark,
                    pinCode: req.body.pinCode,
                    country: req.body.selectedCountry,
                    state: req.body.selectedState,
                    city: req.body.selectedCity
                },
                services: [],
                locations: {
                    lattitude: req.body.latLong.lat,
                    longitude: req.body.latLong.long
                },
                mobileNo1: req.body.mobilenumber
            }
        }, (err, result) => {
            if (err) {
                res.send({
                    success: false,
                    msg: 'some thing is wrong'
                })
            } else {
                if (result.nModified > 0) {
                    res.send({
                        success: true,
                        msg: 'Updated successfully'
                    })
                } else {
                    res.send({
                        success: true,
                        msg: 'everything is updated already'
                    })
                }
            }

        })
    }
    updateServices(req, res) {
        const servicesList = req.body.services;
        const mobileNo1 = req.body.mobileNo1;
        userModel.updateOne({
            mobileNo1: mobileNo1
        }, {
            $set: {
                services: servicesList
            }
        }, (err, result) => {
            if (err) {
                return res.json({
                    success: false,
                    data: err
                });
            } else {
                return res.json({
                    success: true,
                    data: 'Services Updated'
                });
            }
        })
    }
    pickLatestOtp(req, res) {
        console.log(req.body)
        otpModel.findOne({
            otpSentOn: req.body.number
        }, (err, result) => {
            if (err) {
                return res.json({
                    success: false
                })
            } else {
                return res.json({
                    success: true,
                    otp: result.otp,
                    number: result.otpSentOn
                })
            }
        })
    }
}
module.exports = {
    user: new User()
}