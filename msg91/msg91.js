const http = require("https");

class MsgSender {
    sendOTP(phoneNumber, countryCode, otp) {
        var options = {
            "method": "POST",
            "hostname": "api.msg91.com",
            "port": null,
            "path": `https://api.msg91.com/api/v5/otp?invisible=1&otp=${otp}&authkey=306919AQlPawkHgfb5de77ee5&mobile=+${phoneNumber}&template_id=5df11a3bd6fc05639622f7e1`,
            "headers": {
                "content-type": "application/json"
            }
        };
        console.log('options :', options)

        var req = http.request(options, function (res) {
            var chunks = [];

            res.on("data", function (chunk) {
                chunks.push(chunk);
            });

            res.on("end", function () {
                var body = Buffer.concat(chunks);
                const response = JSON.parse(body.toString());
            });
        });
        req.end();        
    }
}
module.exports = {
    MsgSender: new MsgSender()
}