const mongoose = require('mongoose');
const server = '127.0.0.1:27017';
const dbname = 'servizioDB';
mongoose.Promise = global.Promise;

class Database {
    constructor() {
        this._connect();
    }
    _connect() {
        mongoose.connect(`mongodb://${server}/${dbname}`, { useNewUrlParser: true, useUnifiedTopology: true })
            .then(() => {
                console.log('dbconnection is successful.');
            })
            .catch(() => {
                console.log('oops something is wrong with Database.');
            });
    }
}

module.exports = { Database: Database }