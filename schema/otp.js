const mongoose = require('mongoose');
const Schema = mongoose.Schema

const OtpSchema = new Schema({
    userId: String,
    otpSentOn: Number,
    otp: Number,
    optTimestamp: Number,
    otpexpiredOn: Number
})

module.exports = {
    OtpSchema: mongoose.model('otps', OtpSchema)
}

