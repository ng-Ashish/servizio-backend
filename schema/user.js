const mongoose = require('mongoose');
const Schema = mongoose.Schema

const UserSchema = new Schema({
    firstName: String,
    lastName: String,
    mobileNo1: String,
    mobileNo2: Number,
    Adharcard: {
        name: String,
        number: String
    },
    Address: {
        businessAddress: String,
        landMark: String,
        pinCode: String,
        country: String,
        state: String,
        city: String
    },
    isVender: { type: Boolean, default: false },
    services: [{
        name: String
    }],
    locations: {
        lattitude: Number,
        longitude: Number
    },
    serviceDetailAddress: String,
    userProfile: String,
    status: { type: Boolean, default: false },
    createdOn: { type: Number, default: new Date().getTime() },
    updatedOn: { type: Number, default: new Date().getTime() }
})

module.exports = {
    UserSchema: mongoose.model('users', UserSchema)
}

