const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const port = 5555;
app.use(bodyParser.json({ limit: '1000mb' }))
app.use(bodyParser.urlencoded({ limit: '1000mb', extended: true }))
app.use(cors({ credentials: true, origin: true }))
app.use(express.static('./images'));
/**
 * Database Connection.
 */
const db = require('./dbconn/database').Database;
new db();

/**
 * Application routing.
 */
const userRoute = require('./routes/user');
const serviceRoute = require('./routes/services');
app.use('/api', userRoute.router);
app.use('/api', serviceRoute.router)

/**
 * Server Start
 */
app.listen(port, () => {
    console.log(`port start on ${port}`)
})

