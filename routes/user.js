const router = require('express').Router();
const userController = require('../controller/user').user;

router.post('/user/userRegistration', userController.userRegistration);
router.post('/user/resendOtp', userController.resendOtp);
router.post('/user/verifyotp', userController.verifyOtp);
router.get('/test', userController.test);
router.post('/user/updateuser', userController.updateUser);
router.post('/user/updateServices', userController.updateServices);
router.post('/user/pickotp', userController.pickLatestOtp);

module.exports = {
    router: router
}