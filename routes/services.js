const router = require('express').Router();
const services = require('../controller/services');

router.post('/service/findService', services.findServices);
router.post('/service/findServiceByLocation',services.findServiceByLocation);

module.exports = {
    router: router
}